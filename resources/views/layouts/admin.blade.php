<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="{{ app()->getLocale() }}">
<!--<![endif]-->

<head>
    @include('partials.admin.head')
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
<div class="react-root">
    <div id="navigation">
    </div>
    <div class="app-body">
        <div id="sidebar-navigation">
        </div>
        <main class="main">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Home</li>
            </ol>
            <div class="container-fluid">
                <div id="root">

                </div>
                {{--@yield('content')--}}
            </div>
        </main>
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/admin.js?v=3') }}"></script>

</body>
</html>