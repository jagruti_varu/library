@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if(Auth::user())
                    @if(!Auth::user()->is_admin)
                        <ul class="list-group">
                            <li class="list-group-item"><strong><a href="#">Deshbord</a></strong><br></li>
                            <li class="list-group-item"><strong><a href="{{route('Student.GetRequestList')}}">Your Requests</a></strong><br></li>
                            <li class="list-group-item"><strong><a href="#">Make New Requests</a></strong><br></li>
                            <li class="list-group-item"><strong><a href="#">Search E-Matirial</a></strong><br></li>
                            <li class="list-group-item"><strong><strong><a href="#">New
                                            arivals</a></strong><br></strong><br>
                            </li>
                        </ul>
                    @else
                        <ul class="list-group">
                            <li class="list-group-item"><strong><a href="#">Deshbord</a></strong><br></li>
                            <li class="list-group-item"><strong><a href="#">Students</a></strong><br></li>
                            <li class="list-group-item"><strong><a href="#">View Requests</a></strong><br></li>
                            <li class="list-group-item"><strong><a href="#">Search E-Matirial</a></strong><br></li>
                            <li class="list-group-item"><strong><strong><a href="#">New arivals</a></strong><br></strong><br>
                            </li>
                        </ul>
                    @endif
                @endif
            </div>
        </div>
    </div>
@endsection
