<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- API Token -->
<meta name="api-token" content="{{ (Auth::user()) ? Auth::user()->api_token : '' }}">

<title>{{ (isset($pageTitle)) ? ($pageTitle).  ' | Libraries' : 'Libraries' }}</title>

<!-- Favicon -->
{{--<link rel="shortcut icon" href="/images/favicon.png" type="image/png">--}}


{{--<!-- Styles -->--}}
{{--<link href="{{ asset('css/admin.css?v=3') }}" rel="stylesheet">--}}