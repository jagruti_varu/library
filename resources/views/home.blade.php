@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if(Auth::user())
                @if(!Auth::user()->is_admin)
                    <ul class="list-group">
                        <li class="list-group-item"><strong><a href="#">Dashbord</a></strong><br></li>
                        <li class="list-group-item"><strong><a href="{{route('Student.GetRequestList')}}">Your Requests</a></strong><br></li>
                        <li class="list-group-item"><strong><a href="#">Make New Requests</a></strong><br></li>
                        <li class="list-group-item"><strong><a href="#">Search E-Matirial</a></strong><br></li>
                        <li class="list-group-item"><strong><strong><a href="#">New
                                        arivals</a></strong><br></strong><br>
                        </li>
                    </ul>
                @else
                    <ul class="list-group">
                        <li class="list-group-item"><strong><a href="#">Dashbord</a></strong><br></li>
                        <li class="list-group-item"><strong><a href={{route('Admin.GetStudentList')}}>Students</a></strong><br></li>
                        <li class="list-group-item"><strong><a href="{{route('Admin.GetRequestList')}}">View Requests</a></strong><br></li>
                        <li class="list-group-item"><strong><a href="#">Search E-Matirial</a></strong><br></li>
                        <li class="list-group-item"><strong><a href="#">Upload Matirial</a></strong><br></li>
                        <li class="list-group-item"><strong><strong><a href="#">New arivals</a></strong><br></strong><br>
                        </li>
                    </ul>
                @endif
                    @else
                    <strong>welcome please login to view or download library matirials..!!</strong>
                    @endif
            </div>
        </div>
    </div>
@endsection
