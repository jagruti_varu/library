@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                   <div class="card-header">Students</div>
                    <div class="card-body">
                        @if(count($students))
                            <table class="table table-hover">
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                @foreach($students as $student)
                                    @if($student->is_admin)
                                        <tr bgcolor="#ffdab9">
                                    @else
                                        <tr>
                                            @endif
                                            <td>{{$student->id}}</td>
                                            <td>{{$student->name}}</td>
                                            <td>
                                                <a href="{{route('Admin.GetUpdateStudent',$student->id)}}">
                                                    <button type="submit" class="btn btn-success">Edit
                                                    </button>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{route('Admin.GetDeleteStudent',$student->id)}}">
                                                    @if($student->is_admin)
                                                        <button type="submit" class="btn btn-danger"
                                                                disabled> Delete
                                                        </button>
                                                    @else
                                                        <button id="{{'button'}}"
                                                                onclick="{return ConfirmDelete();}" type="submit"
                                                                class="btn btn-danger">Delete
                                                        </button>
                                                    @endif
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                            </table>
                        @endif
                        {{ $students->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function ConfirmDelete() {
            this.disabled = true;
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }
    </script>
@endsection