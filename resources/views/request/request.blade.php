@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Request') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('Auth.PostRegister') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="title"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label><span
                                        class="danger" style="color: red"><h3>*</h3></span>

                                <div class="col-md-6">
                                    @if($request->title)
                                        <input type="hidden" value="{{$request->id}}" name="id">
                                        <input width="200px" id="title" type="text"
                                               class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                               name="title" value="{{ $request->title }}" required autofocus>
                                    @else
                                        <input id="title" type="text"
                                               class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                               name="title" value="{{ old('title') }}" required autofocus>
                                    @endif
                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="type" class="col-md-4 col-form-label text-md-right">{{ __('Type') }}</label>
                                <select name="status" class="custom-select"
                                        style="width:150px; margin-left: 20px ">
                                    <option class="glyphicon-option-horizontal" value="artical">Article</option>
                                    <option class="glyphicon-option-horizontal" value="Ebook">Ebook</option>
                                </select>
                                <div class="col-md-6">

                                    @if ($errors->has('type'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="topic"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Topic') }}</label>

                                <div class="col-md-6">
                                    @if($request->topic)
                                        <input id="topic" type="text"
                                               class="form-control{{ $errors->has('topic') ? ' is-invalid' : '' }}"
                                               name="topic" value="{{$request->topic }}" autofocus>
                                    @else
                                        <input id="topic" type="text"
                                               class="form-control{{ $errors->has('topic') ? ' is-invalid' : '' }}"
                                               name="topic" value="{{ old('topic') }}" autofocus>
                                    @endif
                                    @if ($errors->has('topic'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('topic') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="author"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Author') }}</label>

                                <div class="col-md-6">
                                    @if($request->author)
                                        <input id="author" type="text"
                                               class="form-control{{ $errors->has('author') ? ' is-invalid' : '' }}"
                                               name="author" value="{{ $request->author }}" autofocus>
                                    @else
                                        <input id="author" type="text"
                                               class="form-control{{ $errors->has('author') ? ' is-invalid' : '' }}"
                                               name="author" value="{{ old('author') }}" autofocus>
                                    @endif
                                    @if ($errors->has('author'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('author') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        @if($request)
                                            {{ __('Update') }}
                                        @else
                                            {{ __('Create') }}
                                        @endif
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
