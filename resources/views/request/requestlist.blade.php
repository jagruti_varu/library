@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <br><br>
                <div class="card">
                    <div class="card-header"><strong>{{ __('Requests') }}</strong></div>
                    <div class="card-body">

                        <form method="GET" action="{{ route('Admin.GetSerchRequest') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="status"
                                       class="col-md-2 col-form-label text-md-right">{{ __('Status') }}</label>

                                <div class="col-md-5">
                                    <select name="status" class="custom-select"
                                            style="width:150px; margin-top:5px;  margin-right:4px; ">
                                        <option value="all" selected>All</option>
                                        <option value="pending">Pending</option>
                                        <option value="deleted">Deleted</option>
                                        <option value="accepted">Accepted</option>
                                        <option value="completed">Completed</option>
                                    </select>
                                    <button type="submit" class="btn btn-primary">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true">Search</span>
                                    </button>
                                    @if ($errors->has('status'))
                                        <span class="invalid-feedback">

                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </form>
                        <br>
                        <table class="table table-hover table-bordered">
                            {{--class="table table-condensed">--}}
                            <tr>
                                <th>Request Title</th>
                                <th>Status</th>
                            </tr>
                            @if(count($requests))
                                @foreach($requests as $request)
                                    <tr>
                                        @if(Auth::user()->is_admin)
                                            <td><strong><a href="{{route('Admin.GetRequest',$request->id)}}">{{$request->title}}</a></strong>
                                            </td>
                                        @else
                                            <td><strong><a href="{{route('Student.GetRequest',$request->id)}}">{{$request->title}}</a></strong>
                                            </td>
                                        @endif
                                        <td>
                                            @if ($request->status === 'completed')
                                                <span class="mj_btn btn btn-primary">Completed</span>
                                            @elseif ($request->status === 'deleted')
                                                <span class="mj_btn btn btn-danger">Deleted</span>
                                            @elseif ($request->status === 'accepted')
                                                <span class="mj_btn btn btn-success">Accepted</span>
                                            @else
                                                <span class="mj_btn btn btn-warning">Pending</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                        {{--{{ $requests->links() }}--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection