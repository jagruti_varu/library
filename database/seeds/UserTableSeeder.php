<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $now = Carbon::now()->toDateTimeString();
        $request = [];
        $status=['pending','completed','deleted','accepted'];
        $users=\App\Domain\User\User::query()->pluck('id');
        $request[]=[
            'name'=>$faker->name,
            'email'=>'admin@gmail.com',
            'registration_number'=>$faker->numberBetween(1111,9999),
            'password'=>bcrypt('123456'),
           // 'is_admin'=>1,
            'created_at' => $now,
            'updated_at' => $now,
        ];
        for($i=1;$i<=50;$i++)
        {
            $request[]=[
                'name'=>$faker->name,
                'email'=>'user'.$i.'@gmail.com',
                'registration_number'=>$faker->numberBetween(1111,9999),
                'password'=>bcrypt('123456'),
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        \App\Domain\User\User::query()->insert($request);

    }
}
