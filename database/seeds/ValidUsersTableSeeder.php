<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ValidUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();
        $users = [];
        $branch=['BE', 'ME', 'MBA', 'MCA'];
        $users[]=[
            'email'=>'admin@mail.com',
            'registration_number'=>'11',
            'branch'=>$branch[mt_rand(0,3)],
            'created_at' => $now,
            'updated_at' => $now,
        ];
        for($i=1;$i<=50;$i++)
        {
            $users[]=[
                'email'=>'user'.$i.'@mail.com',
                'registration_number'=>'10305151'.$i,
                'branch'=>$branch[mt_rand(0,3)],
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        \App\Domain\User\ValidUsers::query()->insert($users);
    }
}
