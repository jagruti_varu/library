<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RequestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $now = Carbon::now()->toDateTimeString();
        $request = [];
        $status = ['pending', 'completed', 'deleted', 'accepted'];
        $users = \App\Domain\User\User::query()->pluck('id');
        for ($i = 1; $i <= 50; $i++) {
            $noOfEntries = mt_rand(3, 10);
            for ($j = 0; $j <= $noOfEntries; $j++) {
                $request[] = [
                    'type' => $faker->word,
                    'topic' => $faker->word,
                    'title' => $faker->sentence(),
                    'author' => $faker->name,
                    'user_id' => $users[$i],
                    'status' => $status[mt_rand(0, sizeof($status) - 1)],
                    'created_at' => $now,
                    'updated_at' => $now,
                ];
            }
        }
        \App\Domain\Request\Request::query()->insert($request);
    }
}
