<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValidUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('valid_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('registration_number')->unique();
            $table->enum('branch',['BE', 'ME', 'MBA', 'MCA']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('valid_users');
    }
}
