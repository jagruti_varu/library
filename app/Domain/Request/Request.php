<?php

namespace App\Domain\Request;

use App\Domain\Base\Request\BaseRequest;

class Request extends BaseRequest
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'topic', 'title', 'author', 'user_id', 'status',
    ];
}
