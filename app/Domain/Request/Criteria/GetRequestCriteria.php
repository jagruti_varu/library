<?php

namespace App\Domain\Request\Criteria;

use Levaral\Core\Criteria\BaseCriteria;

class GetRequestCriteria extends BaseCriteria
{
    /**
     * @var integer
     */
    public $id;
}