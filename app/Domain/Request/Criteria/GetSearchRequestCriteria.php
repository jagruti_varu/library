<?php

namespace App\Domain\Request\Criteria;

use App\Domain\User\User;
use Levaral\Core\Criteria\BaseCriteria;

class GetSearchRequestCriteria extends BaseCriteria
{
    /**
     * @var string
     */
    public $status=null;

    /**
     * @var User
     */
    public $user;
}