<?php

namespace App\Domain\Request;

use App\Domain\Request\Criteria\GetRequestCriteria;
use App\Domain\Request\Criteria\GetSearchRequestCriteria;
use Illuminate\Support\Facades\Auth;

class RequestSevice
{
    public function getRequestList()
    {
        if (!Auth::user()->is_admin) {
            return Request::query()->where('user_id', Auth::user()->id)->get();
        }
        $requests = Request::query()->paginate(10);
        return $requests;
    }

    public function getRequestListByStatus(GetSearchRequestCriteria $criteria)
    {
        $user = $criteria->user;
        if (!$user->getIsAdmin()) {
            if ($criteria->status === "all") {
                return Request::query()->where('user_id', Auth::user()->id)->get();
            }
            return Request::query()->where('user_id', Auth::user()->id)->where('status', $criteria->status)->get();
        }
        if ($criteria->status === "all") {
            return Request::query()->paginate(10);
        }
        $requests = Request::query()->where('status', $criteria->status)->paginate(10);
        return $requests;
    }

    public function getRequest($id)
    {
        $request = Request::query()->findOrFail($id);
        return $request;
    }


}