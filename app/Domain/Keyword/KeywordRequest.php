<?php
namespace App\Domain\Keyword;

use App\Domain\Base\Keyword\BaseKeywordRequest;

class KeywordRequest extends BaseKeywordRequest {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'request_id','keyword_id'
    ];
}
