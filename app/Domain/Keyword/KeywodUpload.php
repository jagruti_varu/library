<?php

namespace App\Domain\Keyword;

use App\Domain\Base\Keyword\BaseKeywodUpload;

class KeywodUpload extends BaseKeywodUpload
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'keyword_id','file_id',
    ];


}
