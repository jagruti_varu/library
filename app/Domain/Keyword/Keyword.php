<?php
namespace App\Domain\Keyword;

use App\Domain\Base\Keyword\BaseKeyword;

class Keyword extends BaseKeyword {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'keyword'
    ];

}
