<?php
namespace App\Domain\User;

use App\Domain\Base\User\BaseValidUsers;

class ValidUsers extends BaseValidUsers {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'registration_number', 'branch','email',
    ];
}
