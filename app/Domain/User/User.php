<?php

namespace App\Domain\User;

use App\Domain\Base\User\BaseUser;
use Illuminate\Notifications\Notifiable;

class User extends BaseUser
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'registration_number', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}
