<?php

namespace App\Domain\User;

use App\Domain\User\DTO\ResetPasswordDTO;
use App\Domain\User\DTO\StudentDTO;
use App\Domain\User\DTO\UserRegisterDTO;
use Illuminate\Support\Facades\Password;

class UserService
{
    public function getList()
    {
        return User::query()->where('is_admin',0)->paginate(10);
    }
    public function getStudent($id)
    {
        return User::query()->where('is_admin',0)->findOrFail($id);
    }
    public function create(UserRegisterDTO $userRegisterDTO)
    {
        $user = new User();
        $user->setEmail($userRegisterDTO->email);
        $user->setName($userRegisterDTO->name);
        $user->setPassword(bcrypt($userRegisterDTO->password));
        $user->is_admin=$userRegisterDTO->is_admin;
        $user->registration_number=$userRegisterDTO->registration_number;
        $user->save();
        return $user;
    }

    public function update(StudentDTO $studentDTO)
    {
        $user = User::query()->find($studentDTO->id);
        $user->setEmail($studentDTO->email);
        $user->setName($studentDTO->name);
        $user->setPassword(bcrypt($studentDTO->password));
        $user->is_admin=$studentDTO->is_admin;
        $user->registration_number=$studentDTO->registration_number;
        $user->save();
        return $user;
    }

    public function delete($id)
    {
         return User::query()->find($id)->delete();
    }
    public function sendResetLinkEmail($email)
    {
        $response = $this->broker()->sendResetLink(
            ['email' => (string)$email]
        );

        if ($response == Password::RESET_LINK_SENT) {
            return true;
        }

        return false;
    }

    public function reset(ResetPasswordDTO $dto)
    {
        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            [
                'email' => $dto->email,
                'password' => $dto->password,
                'password_confirmation' => $dto->password_confirmation,
                'token' => $dto->token
            ],
            function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );
        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        if ($response == Password::PASSWORD_RESET) {
            return true;
        }
        return false;
    }

}