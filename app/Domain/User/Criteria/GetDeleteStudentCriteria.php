<?php

namespace App\Domain\Request\Criteria;

use Levaral\Core\Criteria\BaseCriteria;

class GetDeleteStudentCriteria extends BaseCriteria
{
    /**
     * @var integer
     */
    public $id;
}