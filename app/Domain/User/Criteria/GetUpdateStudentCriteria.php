<?php

namespace App\Domain\Request\Criteria;

use Levaral\Core\Criteria\BaseCriteria;

class GetUpdateStudentCriteria extends BaseCriteria
{
    /**
     * @var integer
     */
    public $id;
}