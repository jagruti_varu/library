<?php


namespace App\Domain\User\DTO;

use Levaral\Core\DTO\BaseDTO;

class UserRegisterDTO extends BaseDTO
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $registration_number;
    /**
     * @var boolean
     */
    public $is_admin=false;


}