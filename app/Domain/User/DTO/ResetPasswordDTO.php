<?php

namespace App\Domain\User\DTO;

use Levaral\Core\DTO\BaseDTO;

class ResetPasswordDTO extends BaseDTO
{
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $password;
    /**
     * @var string
     */
    public $password_confirmation;
    /**
     * @var string
     */
    public $token;
}