<?php

namespace App\Domain\User\DTO;

use Levaral\Core\DTO\BaseDTO;

class AuthDTO extends BaseDTO
{
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $password;
    /**
     * @var boolean
     */
    public $remember = false;
}