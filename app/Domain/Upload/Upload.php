<?php
namespace App\Domain\Upload;

use App\Domain\Base\Upload\BaseUpload;

class Upload extends BaseUpload {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type','topic','title','author','filename'
    ];

}
