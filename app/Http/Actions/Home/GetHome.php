<?php

namespace App\Http\Actions\Home;

use App\Http\Actions\GetAction;

class GetHome extends GetAction
{
    public function __construct()
    {

    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $pageTitle = 'Home';
        return view('home', compact('pageTitle'));
    }
}