<?php

namespace App\Http\Actions\Admin;


use App\Domain\Request\RequestSevice;
use App\Http\Actions\GetAction;

class GetRequestList extends GetAction
{
    protected $requestSevice;
    public function __construct(RequestSevice $requestSevice)
    {
        $this->requestSevice=$requestSevice;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $requests=$this->requestSevice->getRequestList();
        return view('request.requestlist',compact('requests'));
    }
}