<?php

namespace App\Http\Actions\Admin;

use App\Domain\User\UserService;
use App\Http\Actions\PostAction;

class PostDeleteStudent extends PostAction
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'requier|integer'
        ];
    }

    public function execute($id)
    {
        $request = $this->userService->delete($id);
        return redirect()->route('Admin.GetStudentList');
    }
}