<?php

namespace App\Http\Actions\Admin;

use App\Domain\User\UserService;
use App\Http\Actions\GetAction;

class GetStudentList extends GetAction
{
    protected $userService;
    public function __construct(UserService $userService)
    {
        $this->userService=$userService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $students=$this->userService->getList();
        return view('student.studentlist',compact('students'));
    }
}