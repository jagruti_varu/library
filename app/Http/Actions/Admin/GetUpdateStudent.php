<?php

namespace App\Http\Actions\Admin;


use App\Domain\User\UserService;
use App\Http\Actions\GetAction;

class GetUpdateStudent extends GetAction
{
    protected $userService;
    public function __construct(UserService $userService)
    {
        $this->userService=$userService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id'=>'requier|integer'
        ];
    }

    public function execute($id)
    {
        $student=$this->userService->getStudent($id);
        return view('student.student',compact('student'));
    }
}