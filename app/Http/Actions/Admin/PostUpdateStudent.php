<?php

namespace App\Http\Actions\Admin;


use App\Domain\User\DTO\PostUpdateStudentDTO;
use App\Domain\User\DTO\StudentDTO;
use App\Domain\User\DTO\StudentDTOtudentDTO;
use App\Domain\User\UserService;
use App\Http\Actions\PostAction;

class PostUpdateStudent extends PostAction
{
    protected $userService;
    public function __construct(UserService $userService)
    {
            $this->userService=$userService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id'=>'required|integer',
            'email' => 'required|string|email|max:255',
            'name' => 'required|string|max:255',
            'registration_number' => 'required|string|max:255',
//            'is_admin' =>'required|boolean',
        ];

    }

    public function execute()
    {
        $dto=new StudentDTO($this->data());
        $this->userService->update($dto);
        return redirect()->route('Admin.GetStudentList');
    }
}