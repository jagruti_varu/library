<?php

namespace App\Http\Actions\Admin;

use App\Domain\Request\RequestSevice;
use App\Http\Actions\GetAction;

class GetRequest extends GetAction
{
    protected $requestSevice;
    public function __construct(RequestSevice $requestSevice)
    {
        $this->requestSevice=$requestSevice;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id'=>'requier|integer'
        ];
    }

    public function execute($id)
    {
        $request=$this->requestSevice->getRequest($id);
        return view('request.request',compact('request'));
    }
}