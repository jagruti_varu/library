<?php

namespace App\Http\Actions\Auth;


use App\Http\Actions\GetAction;

class GetLogIn extends GetAction
{
    public function __construct()
    {

    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $pageTitle = 'Login';
        return view('auth.login', compact('pageTitle'));
    }
}