<?php

namespace App\Http\Actions\Auth;


use App\Http\Actions\GetAction;

class GetRegister extends GetAction
{
    public function __construct()
    {

    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $pageTitle = 'Register';
        return view('auth.register', compact('pageTitle', 'type'));
    }
}