<?php

namespace App\Http\Actions\Auth;


use App\Domain\User\DTO\UserRegisterDTO;
use App\Domain\User\UserService;
use App\Http\Actions\PostAction;
use Illuminate\Support\Facades\Auth;

class PostRegister extends PostAction
{
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed|max:255',
            'name'=>'required|string|max:255',
            'registration_number'=>'required|string|max:255|unique:users',
//            'is_admin' =>'required|boolean',
        ];
    }

    public function execute()
    {
        $dto = new UserRegisterDTO($this->data());
        $user = $this->userService->create($dto);
        Auth::login($user);

        return redirect()->route('Home.GetHome');
    }
}