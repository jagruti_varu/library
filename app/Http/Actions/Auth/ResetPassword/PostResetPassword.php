<?php

namespace App\Http\Actions\Auth\ResetPassword;


use App\Domain\User\DTO\ResetPasswordDTO;
use App\Domain\User\UserService;
use App\Http\Actions\PostAction;

class PostResetPassword extends PostAction
{
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'token' => 'required|string',
            'email' => 'required|email|string',
            'password' => 'required|string|confirmed|min:6|max:255',
            'password_confirmation' => 'string'
        ];
    }

    public function execute()
    {
        $dto = new ResetPasswordDTO($this->data());
        if ($this->userService->reset($dto)) {
            return redirect()->route('Auth.GetLogIn');
        }
        $this->request->session()->flash('error', 'Something went wrong');
        return redirect()->back();
    }
}