<?php

namespace App\Http\Actions\Auth\ResetPassword;


use App\Http\Actions\GetAction;

class GetForgotPassword extends GetAction
{
    public function __construct()
    {

    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $pageTitle = 'Forgot Password';
        return view('auth.passwords.email', compact('pageTitle'));
    }
}