<?php

namespace App\Http\Actions\Auth\ResetPassword;


use App\Http\Actions\GetAction;

class GetResetPassword extends GetAction
{
    public function __construct()
    {

    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute($token)
    {
        $pageTitle = 'Reset Password';
        return view('auth.passwords.reset',compact('token','pageTitle'));
    }
}