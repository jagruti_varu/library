<?php

namespace App\Http\Actions\Auth\ResetPassword;


use App\Domain\User\DTO\AuthDTO;
use App\Domain\User\UserService;
use App\Http\Actions\PostAction;
use App\Domain\User\User;

class PostForgotPassword extends PostAction
{
    /**
     * @var UserService
     */
    protected $userService;
    public function __construct(UserService $userService)
    {
        $this->userService=$userService;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|string|email',
        ];
    }

    public function execute()
    {
        $dto = new AuthDTO($this->data());
        if (!User::query()->where('email', $dto->email)->count()) {
            $this->request->session()->flash('error', trans('passwords.user'));
            return redirect()->back();
        }
        if ($this->userService->sendResetLinkEmail($dto->email)) {
            $this->request->session()->flash('status', trans('passwords.sent'));
            return redirect()->back();
        }
        $this->request->session()->flash('error', 'Something went wrong.');
        return redirect()->back();
    }
}