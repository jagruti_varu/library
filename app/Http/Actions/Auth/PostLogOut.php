<?php

namespace App\Http\Actions\Auth;


use App\Http\Actions\PostAction;
use Illuminate\Support\Facades\Auth;

class PostLogOut extends PostAction
{
    public function __construct()
    {

    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        Auth::logout();
        $this->request->session()->invalidate();
        return redirect()->route('Home.GetHome');
    }
}