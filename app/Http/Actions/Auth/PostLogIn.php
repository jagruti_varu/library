<?php

namespace App\Http\Actions\Auth;

use App\Domain\User\User;
use Illuminate\Support\Facades\Auth;
use App\Domain\User\DTO\AuthDTO;
use App\Http\Actions\PostAction;

class PostLogIn extends PostAction
{
    public function __construct()
    {

    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember' => 'boolean'
        ];
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function execute()
    {
        $dto = new AuthDTO($this->data());
        $user = User::query()->where('email', $dto->email)->first();
//        if (!$user) {
//           // return redirect()->back()->withErrors(['email' => trans('auth.failed')])->withInput($this->data());
//        }
        if (Auth::attempt(['email' => $dto->email, 'password' => $dto->password], $dto->remember)) {
            if (!$user->is_admin) {
                return redirect()->route('Student.GetDashboard', ['any' => 'profile/create']);
            }

            return redirect()->route('Admin.GetDashboard', ['any' => 'dashboard']);

        }
        return redirect()->back()->withErrors(['email' => trans('auth.failed')])->withInput($this->data());
    }
}