<?php

namespace App\Http\Actions\Student;


use App\Http\Actions\GetAction;

class GetDashboard extends GetAction
{
    public function __construct()
    {

    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function execute()
    {
        $pageTitle = 'Dashboard';
        return view('home', compact('pageTitle'));
    }
}