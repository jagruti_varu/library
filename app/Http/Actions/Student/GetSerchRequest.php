<?php

namespace App\Http\Actions\Student;


use App\Domain\Request\Criteria\GetSearchRequestCriteria;
use App\Domain\Request\RequestSevice;
use App\Http\Actions\GetAction;

class GetSerchRequest extends GetAction
{
    protected $requestSevice;
    public function __construct(RequestSevice $requestSevice)
    {
        $this->requestSevice=$requestSevice;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'status'=>'required|string'
        ];
    }

    public function execute()
    {
        $criteria = new GetSearchRequestCriteria($this->data());
        $requests=$this->requestSevice->getRequestListByStatus($criteria);
        return view('request.requestlist',compact('requests'));
    }
}