<?php

use Levaral\Core\Action\Action;

Action::get('/', App\Http\Actions\Home\GetHome::class);

Route::middleware(['guest'])->group(function () {
    Action::get('login', \App\Http\Actions\Auth\GetLogIn::class);
    Action::post('login', \App\Http\Actions\Auth\PostLogIn::class);
    Action::get('password/reset', \App\Http\Actions\Auth\ResetPassword\GetForgotPassword::class);
    Action::post('password/email', \App\Http\Actions\Auth\ResetPassword\PostForgotPassword::class);
    Action::get('password/reset/{token}', \App\Http\Actions\Auth\ResetPassword\GetResetPassword::class);
    Action::post('password/reset', \App\Http\Actions\Auth\ResetPassword\PostResetPassword::class);
    Action::get('/register', App\Http\Actions\Auth\GetRegister::class);
    Action::post('/register', App\Http\Actions\Auth\PostRegister::class);

});

Route::middleware(['auth'])->group(function () {
    Action::get('admin/requests', App\Http\Actions\Admin\GetRequestList::class);
    Action::get('admin/requests/status', App\Http\Actions\Admin\GetSerchRequest::class);
    Action::get('admin/request/{id}', App\Http\Actions\Admin\GetRequest::class);
    Action::get('admin/students', App\Http\Actions\Admin\GetStudentList::class);
    Action::post('admin/student/update', App\Http\Actions\Admin\PostUpdateStudent::class);
    Action::get('admin/student/update/{id}', App\Http\Actions\Admin\GetUpdateStudent::class);
    Action::post('admin/student/delete/{id}', App\Http\Actions\Admin\PostDeleteStudent::class);

    Action::get('student/requests', App\Http\Actions\Student\GetRequestList::class);
    Action::get('student/requests/status', App\Http\Actions\Student\GetSerchRequest::class);
    Action::get('student/request/{id}', App\Http\Actions\Student\GetRequest::class);

    Route::get('student/{any}', App\Http\Actions\Student\GetDashboard::class)
        ->name('Student.GetDashboard')->where('any', '.*');
    Route::get('admin/{any}', App\Http\Actions\Admin\GetDashboard::class)
        ->name('Admin.GetDashboard')->where('any', '.*');

//Action::get('support',\App\Http\Actions\Page\GetSupport::class);
//Action::get('contact',\App\Http\Actions\Page\GetContactUs::class);
//Action::post('contact',\App\Http\Actions\Page\PostContactUs::class);
    Action::post('logout', \App\Http\Actions\Auth\PostLogOut::class);
});